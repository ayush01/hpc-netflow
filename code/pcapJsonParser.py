#After extraction of caida-pcap data using pcapextract.py, 
#features values are extracted as csv file using this script.
import sys
import os
import ast
from itertools import islice
import operator
import csv
import collections
from operator import add
import pandas as pd
import copy
import numpy as np
import time
import datetime
import calendar
import json
import ast

#BEGIN-CONSTANTS============================================================
    #lists that are mapped to the processed file.
    #Lists are in the order feature-values appear in the processed file.
F_TIMES =["starttime","prevts"]
##count total-no-tcppkts and total-no-udppkts (Summing on all the tcp and udp port's counts)
#Do not change it.
MAIN_FEATURES = ["pktcount"
                 ,"bytecount" #/pktcount
                 ,"fragcount" #/pktcount
                 ,"pktcountv6"
                 ,"bytecountv6" #/pktcount-V6
                 
                 ,"lcttl" #/pktcount
                 ,"lcsip" #/pktcount
                 ,'lcdip' #/pktcount
                 ,"lcmsbip" #pktcount                 
                 ,"lcmsb2ip" #pktcount
                 
                 ,"lctcpcon" #/total-tcppkts
                 ,"lcudpcon" #/total-udppkts
                 
                 ,"cbl" #/total-tcppkts + total-udppkts; byte-lower
                 ,"cbh" #/total-tcppkts + total-udppkts; byte-higher
                 
                 ,"ws0" #/total-tcppkts
                 ,"rsw0" #/total-tcppkts
                 ,"countt_se" #/total-tcppkts
                 ,"count_sap" #/total-tcppkts
                 ,"count_sa"] #/total-tcppkts

LEN_ENTROPIES = ["entr_cttl" #do not need to normalize
                 ,"entr_csip" 
                 ,"entr_cdip"
                 ,"entr_cmsbip","entr_cmsb2ip"]

COLLECT_FEATS_NAMES= {3:'CMS_BIP',
                     4:'CIP_PROT',
                     5:'CIP_TCP_PNUM',
                     6:'CIP_UDP_PNUM',
                     7:'CIP_ICMP',
                     8:'CTCPHS',
                     9:'CTCP_FLAGBYTE',
                     10:'CTCP_FLAG8',
                     11:'CWS_SCALE',
                     12:'CSS'}
#Give names of Required Features 'R' that you want to select from each of the above lists
RF_TIMES = F_TIMES       
RMAIN_FEATURES = MAIN_FEATURES
RLEN_ENTROPIES = list(set(LEN_ENTROPIES) - set(["entr_csip" ,"entr_cdip","entr_cmsb2ip"])) 

#remove CMS_BIP
CMS_BIP =[32]#63, 242, 238, 61, 54] #240, 60, 224, 3, 9, 6, 165, 2, 59, 62, 226, 239, 53, 90, 93, 227, 229, 246, 249, 5, 230, 92, 237, 57, 50, 103, 228, 7, 70, 247, 91, 52, 244, 56, 128]
#/total packets 
CIP_PROT = [6, 17, 50, 1, 47]#4, 51, 101, 46, 41, 108, 255, 0, 88, 80, 57, 103, 132, 97, 2, 99, 18, 54, 249, 63, 45, 138, 250, 147, 21, 235, 111, 251, 252, 3, 5, 8, 9, 14, 145]
#/total-tcppkts
CIP_TCP_PNUM = [1024, 80, 443, 25, 554, 22,119]#, 110, 445, 563, 20, 81, 21, 139, 135, 88, 993, 995, 143, 873, 777, 50, 587, 179, 23, 53, 800, 113, 1000, 1001, 82, 85, 1007, 465, 1003, 308, 83, 1, 646, 959]
#/total-udppkts
CIP_UDP_PNUM = [1024, 53, 80, 500, 123]#79(error),137, 161, 443, 514, 113, 439, 646, 1, 370, 259, 54, 551, 21, 81, 88, 1000, 554, 501, 1010, 2, 135, 1004, 670, 1012, 22, 82, 276, 5, 7, 0, 100, 995, 1020, 1022]
#/total-icmppkts
CIP_ICMP = [8, 3, 0, 11]#5(error), -1, 4, 128, 21, 26, 27, 12, 17, 13, 1, 15, 137, 53, 24, 52, 7, 200, 57, 56, 14, 31, 234, 35, 51, 65, 91, 109, 46, 39, 42, 54, 23, 43, 45, 50]
#include-all
#/total-tcppkts
CTCPHS = [5, 8, 7, 10, 11, 6, 13, 12, 15, 14, 9] # error 0, 1, 4, 3, 2]
#/total-tcppkts
CTCP_FLAGBYTE = [16, 24, 2, 17, 18]# 20, 4, 25, 144, 21, 194, 1, 152, 148, 153, 28, 132, 80, 60, 68, 196, 5, 0, 208, 56, 84, 57, 12, 212, 82, 6, 8, 210, 26, 41, 22, 48, 3, 85, 49]
#include-all
#/tcp-pkts
CTCP_FLAG8 = ['ack', 'psh', 'syn', 'fin', 'rst', 'cwr', 'ece']#'urg'
#include-all
#/total-tcppkts
CWS_SCALE = [2, 3, 1, 8, 7,4, 6, 9, 5, 14, 13, 12, 11, 10, 15, 52, 53, 251, 220] 
#/total-tcppkts, remove these
CSS = [] #1460, 1452, 1380, 1440, 1414 ]#536, 1412, 1430, 1260, 1360, 1400, 1408, 1420, 1340, 1398, 1456, 1436, 1402, 1352, 1300, 1448, 1432, 1442, 1392, 8960, 1404, 1410, 1396, 1280, 1024, 1372, 1426, 1428, 1444, 1364, 1418, 1446, 1406, 1304, 1200]

#entropies for the collection objects are evaluated if included in RENTR_OF_FEATS list.
RENTR_OF_FEATS = list(set(COLLECT_FEATS_NAMES.values()) -set(["CSS"])) #required entr of features

#cmsbip,Cipport,ciptcppnum cipudppnum cipicmp ctcphs  ctcpflagbyte ctcpflag8 cwscale css
#ALIAS OF FEATURES' are
FEAT_ALIAS ={ 3:'CMS_BIP-',
              4:'CIP_PROT-',
              5:'CIP_TCP_PORT-',
              6:'CIP_UDP_PORT-',
              7: 'CIP_ICMP-',
              8:'CTCP_HS-',
              9:'CTCP_FLGBYTE-',
              10:'CTCP_FLG8-',
              11:'CTCP_WS-',
              12:'CTCP_SS-'
            }

#divisor :[dividend1,dividend2]
FACTOR_TABLE ={"PKTCOUNT":['BYTECOUNT',
                           'FRAGCOUNT',
                           'LCTTL',
                           'LCSIP',
                           'LCDIP',
                           'LCMSBIP',
                           'LCMSB2IP']+['CIP_PROT-'+str(protid) for protid in CIP_PROT] 
                                      +['CTCP_HS-'+str(hs) for hs in CTCPHS]
                                      +['CTCP_FLGBYTE-'+str(fb) for fb in CTCP_FLAGBYTE]
                                      +['CTCP_FLG8-'+str(f8).upper() for f8 in CTCP_FLAG8]
                                      +['CTCP_WS-'+str(ws) for ws in CWS_SCALE]
                                      +['CTCP_CSS-'+str(cssid) for cssid in CSS],
               "PKTCOUNTV6":['BYTECOUNTV6'
                            ],
               "CIP_PROT-6":['LCTCPCON',
                             'WS0',
                             'RSW0',
                             'COUNTT_SE',
                             'COUNT_SAP',
                             'COUNT_SA' ]+['CIP_TCP_PORT-'+str(tcpport) for tcpport in CIP_TCP_PNUM ],
               "CIP_PROT-17":['LCUDPCON']+['CIP_UDP_PORT-'+str(udpport) for udpport in CIP_UDP_PNUM ],
               "CIP_PROT-1" :['CIP_ICMP-'+str(icmpport) for icmpport in CIP_ICMP],
               "CIP-PROT-6+17":["CBH","CBL"] #normalized by sum of PROT-6 and PROT-17
               
              }

UDP_PROT_ID = 17
TCP_PROT_ID = 6
ICMP_PROT_ID = 1

CUSTOM_COUNTER_NAMES =["cmsbip" 
                       ,"cipprot"
                       ,"ciptcppnum"
                       ,"cipudppnum",
                       "cipicmp"
                       ,"ctcphs",
                       "ctcpflagbyte"
                       ,"ctcpflag8"
                       ,"cwscale"
                       ,"css"
                        #,"cipv6prot","cipv6udppnum"
                      ] 
INIT_SUMMARY_HEADERS= ['year'
                       ,'mth'
                       ,'city'
                       ,'dir'
                       ,'ts_diff']

#give the header's orders
CUSTOM_HEADER_SEQUENCE = ["pktcountv6","bytecountv6"]
#do not alter the sequence of the items in this list
#do not remove any items in the list. Change the items in each of the 
#lists to make selections.
RSELECTED_FEATURES = [  RF_TIMES,       
                        RMAIN_FEATURES,
                        RLEN_ENTROPIES,
                        CMS_BIP,
                        CIP_PROT,
                        CIP_TCP_PNUM,
                        CIP_UDP_PNUM,
                        CIP_ICMP, 
                        CTCPHS ,
                        CTCP_FLAGBYTE, 
                        CTCP_FLAG8,
                        CWS_SCALE, 
                        CSS,
                        RENTR_OF_FEATS
                        ]

def entropy(ctr):
    a=ctr.values()
    b=sum(a)
    al=np.asarray(a)
    al=al/float(b)
    val=float('%.7f'%(-sum(np.log(al)*al)))
    return val

def processJson(line):
	jsonLine= json.loads(line)
	SELECTED_FEATS = RSELECTED_FEATURES
	#str(jsonLine['STARTTIME']),str(jsonLine['PREVTS']),
  	fvalues_row=[jsonLine['PKTCOUNT'],jsonLine['BYTECOUNT'],jsonLine['LCSIP'],jsonLine['LDIP'],\
	jsonLine['LCMSBIP'],jsonLine['LCMS2BIP'],jsonLine['LCTPCON'],jsonLine['LCUDPCON'],jsonLine['CBL'],jsonLine['CBH'],jsonLine['WS0'],jsonLine['RSW0'],jsonLine['COUNTT_SE'],jsonLine['COUNT_SAP'],\
	jsonLine['COUNT_SA'],jsonLine['ENT_CTTL'],jsonLine['ENT_CSIP'],jsonLine['ENT_CDIP'],jsonLine['ENT_CMSBIP'],jsonLine['ENT_CMSB2IP']]
	#21
	COLLECTION_FEEATS=['CMS_BIP','CIP_PROT','CIP_TCP_PNUM','CIP_UDP_PNUM','CIP_ICMP','CTCPHS','CTCP_FLAGBYTE','CTCP_FLAG8']
  #,'CIP_ICMP','CTCPHS','CTCP_FLAGBYTE']
  #,'CTCP_FLAG8']
  #,'CSS','CWS_SCALE']
	count=0
        print ">> no collection "+ str(len(fvalues_row))
	for x in COLLECTION_FEEATS:
    		d = dict(jsonLine[x])
    		dkeys=d.keys()
        	print d
		print dkeys
		if x.upper() in SELECTED_FEATS[-1]:
		        #print x			
			counter=collections.Counter(d)
			fvalues_row += [entropy(counter)]
     		print fvalues_row
		if count==0:        
			fvalues_row += [d[key] if key in dkeys else 0 for key in CMS_BIP] 	
		if count==1:
			fvalues_row += [d[key] if key in dkeys else 0 for key in CIP_PROT]
    		if count==2:
			fvalues_row += [d[key] if key in dkeys else 0 for key in CIP_TCP_PNUM]
		if count==3:
			fvalues_row += [d[key] if key in dkeys else 0 for key in CIP_UDP_PNUM]  	        
		if count==4:
			fvalues_row += [d[key] if key in dkeys else 0 for key in CIP_ICMP]
		if count==5:
			fvalues_row += [d[key] if key in dkeys else 0 for key in CTCPHS]		
		if count==6:        
			fvalues_row += [d[key] if key in dkeys else 0 for key in CTCP_FLAGBYTE]
  		if count==7:
			fvalues_row += [d[key] if key in dkeys else 0 for key in CTCP_FLAG8]
		#if count==8:
		#	fvalues_row += [d[key] if key in dkeys else 0 for key in CSS]
		#if count==9:
		#	fvalues_row += [d[key] if key in dkeys else 0 for key in CWS_SCALE]				
		count=count+1  
	print fvalues_row
        print len(fvalues_row)
	sys.exit(1)  		
  	return fvalues_row

if __name__ == "__main__":
  '''
  pass
  '''
  path="/home/ayush/Desktop/lab/lab_repo_bitbucket/spark_code/data/sanjose.dirB.20110120-140100_0.5.txt"
  f= open(path,'r')
  line=f.readline()
  print processJson(line)


