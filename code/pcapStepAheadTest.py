from __future__ import print_function
from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.streaming import StreamingContext
from pyspark.sql import SQLContext, Row
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.regression import LinearRegressionWithSGD,LinearRegressionModel
from pyspark.mllib.linalg import Vectors
import time
import json
import parseJson as pJ


sc = SparkContext("local[2]", "ParsingPcap")
modelpath="file:///home/ayush/Desktop/Spark_pcap/json/data/models/"
testpath="file:///home/ayush/Desktop/Spark_pcap/json/data/test/"

ssc = StreamingContext(sc,1)

model = LinearRegressionModel.load(sc, modelpath)

testLines =ssc.textFileStream(testpath)
testDictionaries = testLines.map(lambda line: line.encode("utf-8"))
testDictionaries2=testDictionaries.transform(lambda rdd: rdd.map(pJ.processJson))
testData= testDictionaries2.map(lambda line: LabeledPoint(line[2],line[3:]))

def printRDD(rdd):
	print(rdd.collect())
	labelsAndPreds = rdd.map(lambda p: (p.label, model.predict(p.features)))
	print(labelsAndPreds.collect())
	
testData.foreachRDD(printRDD)

#model.predict(testData.map(lambda lp: (lp.label, lp.features))).pprint()
ssc.start()
#ssc.awaitTermination()
ssc.awaitTerminationOrTimeout(60)
ssc.stop()