# The data is streamed from TCP sockets in this case.

from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.streaming import StreamingContext
from pyspark.sql import SQLContext, Row
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.regression import StreamingLinearRegressionWithSGD
from pyspark.mllib.linalg import Vectors
import time
import json
import pcapJsonParser as pJ

sc = SparkContext("local[2]", "ParsingPcap")
sqlContext = SQLContext(sc)
ssc = StreamingContext(sc,1)

# For training
lines =ssc.socketTextStream("localhost", 11111)
dictionaries = lines.map(lambda line: line.encode("utf-8"))
dictionaries2=dictionaries.transform(lambda rdd: rdd.map(pJ.processJson))
trainingData= dictionaries2.map(lambda line: LabeledPoint(line[0],line[1:]))

numFeatures=72
model = StreamingLinearRegressionWithSGD().setInitialWeights(Vectors.zeros(numFeatures))
model.trainOn(trainingData)
model.predictOnValues(trainingData.map(lambda lp: (lp.label, lp.features))).pprint()

ssc.start()
#ssc.awaitTermination()
ssc.awaitTerminationOrTimeout(60)
ssc.stop()




