# This file generates json from pcap.gz file
import os,sys
import dpkt
import math
import collections
import numpy
import gzip
import multiprocessing
from multiprocessing import Pool
import time
import json

def entropy(ctr):
    a=ctr.values()
    b=sum(a)
    al=numpy.asarray(a)
    al=al/float(b)
    return -sum(numpy.log(al)*al)

def parseTCPOpts(b):
    i=0
    ss=0
    wscale=0
    sap=0
    sa=0
    ts=0
    n=len(b)
    while i<n:
        if i<n and b[i]==0: i=i+1
        elif i<n and  b[i]==1: i+=1
        elif (i+3)< n and b[i]==2 and b[i+1]==4 :
            ss=b[i+2]*256+b[i+3]
            i+=4
        elif (i+2)<n and b[i]==3 and b[i+1]==3 :
            wscale=b[i+2]
            i+=3
        elif (i+1)<n and b[i]==4 and b[i+1]==2:
            sap=1
            i+=2
        elif i<n and b[i]==5: 
            sa=1
            i=n
        elif i<n and b[i]==8:
            ts=1
            i=n
        else: break
    return ss,wscale,sap,sa,ts

def getfrompacket(b,blen):
    v6=v4=prot=psize=sport=dport=os=ttl=sip=dip=frag=hs=ws=fb=ss=wscale=sap=sa=tse=icmptype=-1
    if  (b[0] >>4)== 6: #first four bits of first byte (ip v6)
        v6=1
        prot=b[6]
        psize=(b[4]<<8) + b[5]
        if b[6]==58 and blen>40: #ICMP
            icmptype=b[40]
        if ((b[6]==6) or (b[6]==17)) and blen>43: #TCP or UDP
            sport=(b[40]<<8)+b[41]
            dport=(b[42]<<8)+b[43]
    if  (b[0]>>4)==4 and blen>19: #first four bits of first byte (ip v4)
        v4=1
        prot=b[9]
        psize=(b[2]<<8) + b[3]
        #os=(b[6]<<3)+b[7]
        os=b[7]
        ttl=b[8]
        sip=(b[12]<<24)+(b[13]<<16)+(b[14]<<8)+b[15]
        dip=(b[16]<<24)+(b[17]<<16)+(b[18]<<8)+b[19]
        if b[9]==1 and blen>23: #ICMP
            icmptype=b[20]
        if ((b[9]==6) or (b[9]==17)) and blen>23: #TCP or UDP
            sport=(b[20]<<8)+b[21]
            dport=(b[22]<<8)+b[23]
        if ((blen>39) and (b[9]==6)): #TCP
            hs=b[32]>>4
            ws=(b[34]<<8)+b[35]
            fb=b[33]
            if hs>5 and blen>40:
                ss,wscale,sap,sa,tse=parseTCPOpts(b[40:])
    return v6,v4,prot,psize,sport,dport,os,ttl,sip,dip,hs,ws,fb,ss,wscale,sap,sa,tse,icmptype
    
def processfile(fname):
    prevts=0
    slicecount=0
    cipv6prot=collections.Counter() 
    cipprot=collections.Counter() 
    cipv6tcppnum=collections.Counter() 
    ciptcppnum=collections.Counter() 
    cipv6udppnum=collections.Counter() 
    cipudppnum=collections.Counter() 
    cipv6icmp=collections.Counter() 
    cipicmp=collections.Counter() 
    cttl=collections.Counter() 
    csip=collections.Counter() 
    cdip=collections.Counter() 
    cmsbip=collections.Counter() 
    cmsb2ip=collections.Counter() 
    ctcpcon=collections.Counter() 
    cudpcon=collections.Counter() 
    ctcphs=collections.Counter() 
    ctcpflagbyte=collections.Counter() 
    ctcpflag8=collections.Counter() 
    ctemp=collections.Counter() ;
    css=collections.Counter() 
    cwscale=collections.Counter()
    count=pktcount=bytecount=fragcount=pktcountv6=bytecountv6=countsap=countsa=counttse=cbl=cbh=ws0=rsw0=0
    print fname
    outfname=fname[-45:-17]
    print outfname
    outfname=outdir+outfname+'_'+str(slicetime)+'.txt'
    #if os.path.isfile(outfname): return #already done!
    fout = open(outfname,'w')
    print outfname
    fin=gzip.open(fname)
    pcap=dpkt.pcap.Reader(fin)
    start_aggregation=time.time()
    for ts,buf in pcap:
        prevts=ts
        b=bytearray()
        b.extend(buf)
        blen=len(b)
        if blen>19: 
            v6,v4,prot,psize,sport,dport,os,ttl,sip,dip,hs,ws,fb,ss,wscale,sap,sa,tse,icmptype=getfrompacket(b,blen)
        #else: print 'bad packet', count
        count+=1
        if count==1: starttime=ts
        if count>1 and ts>(starttime+slicetime):
            row=dict()
            slicecount+=1
            #print slicecount,starttime,prevts
            #print>>fout, "{0:.10f}".format(starttime), "{0:.10f}".format(prevts)
            row['STARTTIME']= "{0:.10f}".format(starttime)
            row['PREVTS']="{0:.10f}".format(prevts)
            #print>>fout,pktcount,bytecount,fragcount,pktcountv6,bytecountv6,len(cttl),len(csip),len(cdip),len(cmsbip),l
            #        en(cmsb2ip),len(ctcpcon),len(cudpcon),cbl,cbh,ws0,rsw0,counttse,countsap,countsa
            row['PKTCOUNT']=pktcount
            row['BYTECOUNT']=bytecount
            row['BYTECOUNTV6']=bytecountv6
            row['PKTCOUNTV6']=pktcountv6
            row['LCTTL']=len(cttl)            
            row['LCSIP']=len(csip)
            row['LCDIP']=len(cdip)
            row['LCMSBIP']=len(cmsbip)
            row['LCMS2BIP']=len(cmsb2ip)
            row['LCTCPCON']=len(ctcpcon)
            row['LCUDPCON']=len(cudpcon)
            row['CBL']=cbl
            row['CBH']=cbh
            row['WS0']=ws0
            row['RSW0']=rsw0
            row['COUNTT_SE']=counttse
            row['COUNT_SAP']=countsap
            row['COUNT_SA']=countsa
            #print>>fout,entropy(cttl),entropy(csip),entropy(cdip),entropy(cmsbip),entropy(cmsb2ip)
            row['ENT_CTTL']=entropy(cttl)
            row['ENT_CSIP']=entropy(csip)
            row['ENT_CDIP']=entropy(cdip)
            row['ENT_CMSBIP']=entropy(cmsbip)
            row['ENT_CMSB2IP']=entropy(cmsb2ip)

            #print>>fout,cmsbip.items()
            row['CMS_BIP']=cmsbip.items()
            #print>>fout,cipprot.items()
            row['CIP_PROT']=cipprot.items()
            #print>>fout,ciptcppnum.items()
            row['CIP_TCP_PNUM']=ciptcppnum.items()
            #print>>fout,cipudppnum.items()
            row['CIP_UDP_PNUM']=cipudppnum.items()
            #print>>fout,cipicmp.items()
            row['CIP_ICMP']=cipicmp.items()
            #print>>fout,ctcphs.items()
            row['CTCPHS']=ctcphs.items()
            #print>>fout,ctcpflagbyte.items()
            row['CTCP_FLAGBYTE']=ctcpflagbyte.items()
            #print>>fout,ctcpflag8.items()
            row['CTCP_FLAG8']=ctcpflag8.items()
            #print>>fout,cwscale.items()
            row['CWS_SCALE']=cwscale.items()
            #print>>fout,css.items()
            row['CSS']=css.items()
            #print>>fout,pktcountv6,bytecountv6
            row['PKTCOUNTV6']=pktcountv6
            row['BYTECOUNTV6']=bytecountv6
            #print>>fout,cipv6prot.items()
            row['CIPV6PROT']=cipv6prot.items()
            #print>>fout,cipv6tcppnum.items()
            row['CIPV6TCPPNUM']=cipv6tcppnum.items()
            #print>>fout,cipv6udppnum.items()
            row['CIPV6UDPPNUM']=cipv6udppnum.items()
            #print>>fout,"$"
            json.dump(row, fout)
            fout.write('\n')
            cttl.clear()
            csip.clear()
            cdip.clear()
            cttl.clear()
            css.clear()
            cmsbip.clear()
            cmsb2ip.clear()
            ctcpcon.clear()
            cudpcon.clear()
            cipprot.clear()
            ciptcppnum.clear()
            cipudppnum.clear()
            cipicmp.clear()
            ctcphs.clear()
            ctcpflagbyte.clear()
            ctcpflag8.clear()
            cwscale.clear()
            cipv6prot.clear()
            cipv6tcppnum.clear()
            cipv6udppnum.clear()
            cipv6icmp.clear()
            pktcount=bytecount=fragcount=pktcountv6=bytecountv6=countsap=countsa=counttse=cbl=cbh=ws0=rsw0=0
            starttime+=slicetime
        if sport>0:
            sp=sport
            if sp>1023: sp=1024
            dp=dport
            if dp>1023: dp=1024
            if sp==1024 and dp==1024: cbh+=1
            if sp<1024 and dp<1024: cbl+=1
        if (v6>0):
            pktcountv6+=1
            bytecountv6+=psize
            cipv6prot[prot]+=1
            if icmptype>-1:
                cipv6icmp[icmptype]+=1
            elif prot==6 and sport>0: 
                cipv6tcppnum[sp]+=1
                cipv6tcppnum[dp]+=1
            elif prot==17 and sport>0: 
                cipv6udppnum[sp]+=1
                cipv6udppnum[dp]+=1
        elif v4>0:
            pktcount+=1
            cipprot[prot]+=1
            bytecount+=psize
            if (ttl >= 0): cttl[ttl]+=1
            csip[sip]+=1
            cdip[dip]+=1
            cmsbip[sip>>24]+=1 
            cmsbip[dip>>24]+=1 
            cmsb2ip[sip>>16]+=1 
            cmsb2ip[dip>>16]+=1 
            if os>0: fragcount+=1
            if prot==1: cipicmp[icmptype]+=1
            if prot==17 and sport>0:
                cipudppnum[sp]+=1 
                cipudppnum[dp]+=1
                cudpcon[(sip,dip,sport,dport)]+=1 
                cudpcon[(dip,sip,dport,sport)]+=1 
            if prot==6 and sport>0:
                if ws==0: ws0+=1 
                if ss>0: css[ss]+=1 
                if wscale>0: cwscale[wscale]+=1 
                if sap>0: countsap+=1
                if sa>0: countsa+=1
                if tse>0: counttse+=1
                ciptcppnum[sp]+=1 
                ciptcppnum[dp]+=1
                ctcpcon[(sip,dip,sport,dport)]+=1 
                ctcpcon[(dip,sip,dport,sport)]+=1
                if hs>-1: ctcphs[hs]+=1
                if fb>-1: 
                    ctcpflagbyte[fb]+=1
                    if (fb&(1<<0)): ctcpflag8['fin']+=1
                    if (fb&(1<<1)): ctcpflag8['syn']+=1
                    if (fb&(1<<2)): ctcpflag8['rst']+=1
                    if (fb&(1<<3)): ctcpflag8['psh']+=1
                    if (fb&(1<<4)): ctcpflag8['ack']+=1
                    if (fb&(1<<5)): ctcpflag8['urg']+=1
                    if (fb&(1<<6)): ctcpflag8['ece']+=1
                    if (fb&(1<<7)): ctcpflag8['cwr']+=1
                    if (fb&(1<<2)) and (ws==0): rsw0+=1 
    if pktcount>0:
        row=dict()
        #print 'done for loop'
        slicecount+=1
        #print slicecount, "{0:.10f}".format(starttime), "{0:.10f}".format(prevts)
        row['STARTTIME']= "{0:.10f}".format(starttime)
        row['PREVTS']="{0:.10f}".format(starttime)
            #print>>fout,pktcount,bytecount,fragcount,pktcountv6,bytecountv6,len(cttl),len(csip),len(cdip),len(cmsbip),l
            #        en(cmsb2ip),len(ctcpcon),len(cudpcon),cbl,cbh,ws0,rsw0,counttse,countsap,countsa
        row['PKTCOUNT']=pktcount
        row['BYTECOUNT']=bytecount
        row['BYTECOUNTV6']=bytecountv6
        row['PKTCOUNTV6']=pktcountv6
        row['LCTTL']=len(cttl)            
        row['LCSIP']=len(csip)
        row['LDIP']=len(cdip)
        row['LCMSBIP']=len(cmsbip)
        row['LCMS2BIP']=len(cmsb2ip)
        row['LCTPCON']=len(ctcpcon)
        row['LCUDPCON']=len(cudpcon)
        row['CBL']=cbl
        row['CBH']=cbh
        row['WS0']=ws0
        row['RSW0']=rsw0
        row['COUNTT_SE']=counttse
        row['COUNT_SAP']=countsap
        row['COUNT_SA']=countsa
            #print>>fout,entropy(cttl),entropy(csip),entropy(cdip),entropy(cmsbip),entropy(cmsb2ip)
        row['ENT_CTTL']=entropy(cttl)
        row['ENT_CSIP']=entropy(csip)
        row['ENT_CDIP']=entropy(cdip)
        row['ENT_CMSBIP']=entropy(cmsbip)
        row['ENT_CMSB2IP']=entropy(cmsb2ip)

            #print>>fout,cmsbip.items()
        row['CMS_BIP']=cmsbip.items()
            #print>>fout,cipprot.items()
        row['CIP_PROT']=cipprot.items()
            #print>>fout,ciptcppnum.items()
        row['CIP_TCP_PNUM']=ciptcppnum.items()
            #print>>fout,cipudppnum.items()
        row['CIP_UDP_PNUM']=cipudppnum.items()
            #print>>fout,cipicmp.items()
        row['CIP_ICMP']=cipicmp.items()
            #print>>fout,ctcphs.items()
        row['CTCPHS']=ctcphs.items()
            #print>>fout,ctcpflagbyte.items()
        row['CTCP_FLAGBYTE']=ctcpflagbyte.items()
            #print>>fout,ctcpflag8.items()
        row['CTCP_FLAG8']=ctcpflag8.items()
            #print>>fout,cwscale.items()
        row['CWS_SCALE']=cwscale.items()
            #print>>fout,css.items()
        row['CSS']=css.items()
            #print>>fout,pktcountv6,bytecountv6
        row['PKTCOUNTV6']=pktcountv6
        row['BYTECOUNTV6']=bytecountv6
            #print>>fout,cipv6prot.items()
        row['CIPV6PROT']=cipv6prot.items()
            #print>>fout,cipv6tcppnum.items()
        row['CIPV6TCPPNUM']=cipv6tcppnum.items()
            #print>>fout,cipv6udppnum.items()
        row['CIPV6UDPPNUM']=cipv6udppnum.items()
            #print>>fout,"$"
        json.dump(row, fout)
        fout.write('\n')
        #print>>fout,"$"
    fin.close()
    fout.close()
    end_aggregation=time.time()
    print end_aggregation-start_aggregation
#end-of-function processfile()

def generate_file_paths(start_dir):
    file_paths=[]
    for p,d,f in os.walk(start_dir):
        for file_name in f:
            if 'UTC.anon.pcap.gz' in file_name and 'tmp' not in file_name:
                file_paths.append(os.path.join(p,file_name))
    return file_paths

def processfile1(fname):
    try:
        processfile(fname)
    except:
	print "LOOK HERE",fname


path="/home/ayush/Desktop/Spark_pcap/testRuntime/equinix-sanjose.dirB.20110120-140100.UTC.anon.pcap.gz"
outdir='/home/ayush/Desktop/Spark_pcap/json/'
slicetime=0.5
processfile(path)

"""
indir = '.'
outdir='/dasi/projects/idaho-bailiff/S5/code/ram/'
flist=os.listdir('.')
slicetime=0.5
for fn in flist:
    if 'pcap.gz' in fn:
        processfile(fn)
#usage python pcapextract.py indir outdir slicetime                
indir=sys.argv[1]
outdir=sys.argv[2]
slicetime=float(sys.argv[3])
print indir,outdir,slicetime
file_paths=generate_file_paths(indir)
print file_paths, indir, outdir, slicetime
num_processes=multiprocessing.cpu_count()
#num_processes=3
print num_processes
processPool=Pool(num_processes)
try:
   results=processPool.map(processfile1,file_paths)
except Exception as e:
    print e 
"""
