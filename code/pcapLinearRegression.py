# This program just directly converts the Linear Regression code that Nraresh did 
# into spark.

from pyspark.sql.types import *
from pyspark.sql import Row
from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.mllib.regression import LabeledPoint, LinearRegressionWithSGD
from pyspark.mllib.util import MLUtils
import parseJson as pJ
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.linalg.distributed import IndexedRow, IndexedRowMatrix
from pyspark.mllib.linalg import DenseMatrix
import time
from pyspark.mllib.linalg.distributed import RowMatrix
from pyspark.mllib.linalg.distributed import Matrix
from pyspark.mllib.linalg import Matrices
import numpy as np
from numpy import linalg as la


def la_norm(X):
    #used by Professor
    m,n = X.shape
    for i in range(n):
        X[:,i]=X[:,i]/la.norm(X[:,i])
    return X

##It is a helper function to makeDataModel3 and SkipJumpsBuildXY
## creates data nh data points in history to predict nf'th data point
def getxy(dt,nh,nf):
    a,b=dt.shape
    if nh == 1:
        X = dt[0:a-nf,:]
        Y = dt[nf:,:]
        return X,Y
    elif nh>1:
        X = dt[0:a-nh-nf+1,:]
        for i in range (1,nh):
            X = np.hstack([X,dt[i:a+i-nh-nf+1,:]])
        Y = dt[nh+nf-1:,:]
        return X,Y
    
##Return X and Y corresponding values for nh and nf
def skipJumpsBuildXY(dt, nh,nf, samplingts):
    tcollidx = 0 #;time column index
    count=0
    curidx=0
    a,b=dt.shape
    tdiff = samplingts + 0.0099
    df=dt[1:,tcollidx]-dt[:-1,tcollidx] #tcollidx is the time index.
    idx=np.where(df>tdiff)
    idxx=idx[0]
    print("total-jumps found"),len(idxx)
    if len(idxx) == 0 :
        if(nh+nf-1) < dt.shape[0]:
            X,Y = getxy(dt,nh,nf)
            return X, Y
        else:
            print("Given nh, nf "),nh,",", nf,"but found n_samples to be", dt.shape[0]
            return None, None
    else:
        for i in range(0, len(idxx)+1):
            if i == len(idxx):
                dtt=dt[curidx:,:]
            else:
                dtt=dt[curidx:idxx[i]+1,:]
                curidx=idxx[i]+1
                
            if(nh+nf-1) < dtt.shape[0]:   
                x,y=getxy(dtt,nh,nf)
                if count==0:
                    X=x
                    Y=y
                elif count>0:
                    if X.shape[1] <> x.shape[1]:
                        continue
                    X=np.vstack((X,x))
                    Y=np.vstack((Y,y))
                count+=1  
            else:
                print("Escaped index:"),i ##there are not enough data to construct x and y using nh and nf
                if i < len(idxx): curidx=idxx[i]+1
                continue            
    return X,Y

def lse(X,Y):
    N,n=X.shape
    NN,ny=Y.shape
    if N != NN:
        return 0
    X,Y = la_norm(X),la_norm(Y)#X,Y = normalize(X,Y,typ=0)
    
    oset = np.zeros([N,1]) ##ignore constant term
    #oset = np.ones([N,1]) ## use constant term
    X = np.hstack((oset,X))
    print la.pinv(X)
    theta = np.dot(la.pinv(X),Y)
    YY = np.dot(X,theta)
    score=[]
    for i in range(ny):
        u=((YY[:,i]-Y[:,i])**2).sum()
        v=((Y[:,i]-np.mean(Y[:,i]))**2).sum()
        score.append(1-u/v)
    return score,theta,YY

#csv = '/work/deepak/spark/ml/data/chicago.dirA.2008_0.5.csv'
path = "file:///home/ayush/Desktop/Spark_pcap/json/data/"
trainpath=path+"train/sanjose.dirB.20110120-140100_0.5.txt"
sc = SparkContext("local[2]", "ParsingPcap")
sqlContext = SQLContext(sc) 

stepahead=4
lines =sc.textFile(trainpath)
filterLines = lines.map(lambda line: line.encode("utf-8"))
jsonLines=filterLines.map(pJ.processJson)
length=len(jsonLines.collect())

nData=np.array(jsonLines.collect())
for x in range(0,74):
    print x
    print nData[:,x]

print nData.shape
X,Y=skipJumpsBuildXY(nData,1,1,1)
print X.shape,Y.shape
xRDD=sc.parallelize(X)
#print(xRDD.collect())
print(type(xRDD))
print ">>>"
score,theta,YY=lse(X,Y)
print score
print ">>"
print theta
print ">"
print YY
print ">> End of Model Creation"
