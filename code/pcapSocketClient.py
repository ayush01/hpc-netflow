
# coding: utf-8

# In[7]:

#!/usr/bin/python           # This is client.py file

import socket               # Import socket module
import struct
import time

s = socket.socket()         # Create a socket object
host = "localhost" 	    # Get local machine name
port = 11111                # Reserve a port for your service.

s.connect((host, port))
print "Started to receive"
startTime=time.time()
while True:
	data=s.recv(8096)
	if not data:
		print('error')
	print data
print "End to receive.. ",time.time()-startTime
s.close()                    # Close the socket when done


# In[ ]:


