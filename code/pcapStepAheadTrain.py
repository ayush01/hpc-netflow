from pyspark.sql.types import *
from pyspark.sql import Row
from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.mllib.regression import LabeledPoint, LinearRegressionWithSGD
from pyspark.mllib.util import MLUtils
import parseJson as pJ
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.linalg.distributed import IndexedRow, IndexedRowMatrix
from pyspark.mllib.linalg import DenseMatrix
import time
import sys
import os

#csv = '/work/deepak/spark/ml/data/chicago.dirA.2008_0.5.csv'
path = "file:///home/ayush/Desktop/Spark_pcap/json/data/"
trainpath=path+"train/sanjose.dirB.20110120-140100_0.5.txt"
sc = SparkContext("local[2]", "ParsingPcap")
sqlContext = SQLContext(sc) 

stepahead=4
lines =sc.textFile(trainpath)
filterLines = lines.map(lambda line: line.encode("utf-8"))
jsonLines=filterLines.map(pJ.processJson)
length=len(jsonLines.collect())
#print jsonLines.collect()

f = jsonLines.zipWithIndex().filter(lambda x: x[1]<(length-stepahead)).keys()
l = jsonLines.zipWithIndex().filter(lambda x: x[1]> (stepahead-1)).keys()

headers=['PKTCOUNT','BYTECOUNT','PKTCOUNTV6','LCSIP','LDIP',\
'LCMSBIP','LCMS2BIP','LCTPCON','LCUDPCON','CBL','CBH','WS0','RSW0','COUNTT_SE','COUNT_SAP',\
 'COUNT_SA','ENTR_CTTL','ENT_CSIP','ENT_CDIP','ENT_CMSBIP','ENT_CMSB2IP',\
 'CMS_BIP','CIP_PROT','CIP_TCP_PNUM','CIP_UDP_PNUM','CIP_ICMP','CTCPHS','CTCP_FLAGBYTE','CTCP_FLAG8','CWS_SCALE']
#print(len(headers))
#print f.collect()
#print l.collect()

numFeatures=30
for x in range(0,2):
	#Features
	ff= f.map(lambda line: line[0:x]+line[x+1:numFeatures])
	#Labels
	ll=l.map(lambda line:line[x:x+1])
	#Merging to create label points
	fff=ff.zipWithIndex().toDF()
	lll=ll.zipWithIndex().toDF()
	fff=fff.withColumnRenamed("_1","_features")
	lll=lll.withColumnRenamed("_1","_labels")
	zzz= fff.join(lll,fff._2 == lll._2)
	trainingData= zzz.map(lambda line: LabeledPoint(line[2][0],line[0]))
	
	#Train
	print x, headers[x]
	#print trainingData.collect()
	starTime=time.time()
	model = LinearRegressionWithSGD.train(trainingData,iterations=100, step=0.0000000000000001)
	print time.time()-starTime
	model.save(sc,path+"models_"+headers[x])
print ">> End of Model Creation"
