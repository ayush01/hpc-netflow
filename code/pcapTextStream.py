from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.streaming import StreamingContext
from pyspark.sql import SQLContext, Row
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.regression import StreamingLinearRegressionWithSGD
from pyspark.mllib.linalg import Vectors
import time
import json
import parseJson as pJ

sc = SparkContext("local[2]", "ParsingPcap")
trainpath="file:///home/ayush/Desktop/Spark_pcap/json/data/train"
testpath="file:///home/ayush/Desktop/Spark_pcap/json/data/test"
sqlContext = SQLContext(sc)
ssc = StreamingContext(sc,1)

# For training
lines =ssc.textFileStream(trainpath)
dictionaries = lines.map(lambda line: line.encode("utf-8"))
dictionaries2=dictionaries.transform(lambda rdd: rdd.map(pJ.processJson))
trainingData= dictionaries2.map(lambda line: LabeledPoint(line[2],line[3:]))

#For testing
testLines =ssc.textFileStream(testpath)
testDictionaries = testLines.map(lambda line: line.encode("utf-8"))
testDictionaries2=testDictionaries.transform(lambda rdd: rdd.map(pJ.processJson))
testData= testDictionaries2.map(lambda line: LabeledPoint(line[2],line[3:]))

numFeatures=29
model = StreamingLinearRegressionWithSGD().setInitialWeights(Vectors.zeros(numFeatures))
trainedModel=model.trainOn(trainingData)

model.predictOnValues(testData.map(lambda lp: (lp.label, lp.features))).pprint()

#model.save(sc,"~/Desktop/testregress")
print type(dictionaries2)
ssc.start()
#ssc.awaitTermination()
ssc.awaitTerminationOrTimeout(60)
ssc.stop()
print "Hello End of Streaming"




