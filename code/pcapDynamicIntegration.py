#this file works on text file streaming input and model creation
from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.streaming import StreamingContext
from pyspark.sql import SQLContext, Row
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.regression import StreamingLinearRegressionWithSGD
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.regression import LinearRegressionWithSGD,LinearRegressionModel
import time
import json
import operator
from collections import Counter
import numpy as numpy
import pcapJsonParser as pJ

def entropy(ctr):
    a=ctr.values()
    b=sum(a)
    al=numpy.asarray(a)
    al=al/float(b)
    return -sum(numpy.log(al)*al)

def rddOperation(rdd):
	if len(rdd.collect())==7:
		print "<<"
		print(len(rdd.collect()))
		df=sqlContext.jsonRDD(rdd)
		
	        # Aggregation of features with numeric value
	        df.registerTempTable("dNumeric")
	        dNumeric=df.select(map(str.upper,pJ.MAIN_FEATURES)) #features list extracted from pcapJsonParser.py
	        ldNumeric=dNumeric.collect()
	        #dNumeric.printSchema()
	        #dNumeric.show()
	        finalFeaturesNumerical=[]
	        nNumericalFeatures=len(pJ.MAIN_FEATURES)
	        
	        for eachColumn in range(0,nNumericalFeatures):
	        	aggFeature=[]
		        for x in range(6,0,-1):
		        	currentRow=ldNumeric[x]
		        	#print currentRow, currentRow[0]
		        	if len(aggFeature)==0:
		        		aggFeature.append(currentRow[eachColumn])
		        	else:
		        		#print aggFeature[-1],currentRow[eachColumn]
		        		aggFeature.append((aggFeature[-1]+currentRow[eachColumn])/2)
		        	#print aggFeature
		        finalFeaturesNumerical.append(aggFeature)
	        ffNumerical=reduce(operator.add,finalFeaturesNumerical)
		
		# Aggregation of dictionary features (entropy)
		df.registerTempTable("dEntropy")
		nEntropyFeatures=len(pJ.LEN_ENTROPIES)
		dEntropy=df.select(map(str.upper,pJ.LEN_ENTROPIES))
		dEntropy.printSchema()
	        dEntropy.show()
		ldEntropy=dEntropy.collect()
		finalEntropyFeatures=[]
		
		for eachColumn in range(0,nEntropyFeatures):
			aggFeature=[]
			ctrFeatures=Counter()
			for x in range(6,0,-1):
				currentRow=ldEntropy[x]
				ctrFeatures.update(dict(currentRow[eachColumn]))
				aggFeature.append(entropy(ctrFeatures))
		        finalEntropyFeatures.append(aggFeature)
		ffEntropy=reduce(operator.add,finalEntropyFeatures)
		
		# Aggregation of collection features
		df.registerTempTable("dCollectionFeats")
		colFeatsName=pJ.COLLECT_FEATS_NAMES.values()
		nCollectionFeats=len(colFeatsName)
		dCollectionFeats=df.select(colFeatsName)
		ldCollectionFeats=dCollectionFeats.collect()
		#dCollectionFeats.printSchema()
		
		# Aggregated values from collection features
		finalCollectionFeatures=[]
		# Entropies of collection features.
		entropyCollectionFeatures=[]
	        for colFeat,index in zip(colFeatsName,list(xrange(len(colFeatsName)))):	        	
	        	keys=pJ.getPorts(colFeat)
	        	if len(keys) !=0:
		        	collFeatures=Counter()
		        	filteredCollection=Counter()
		        	entropies=[]
		        	for i in range(6,0,-1):
		        		currentRow=ldCollectionFeats[i]
		        		# current cell of table
		        		ctrCurrent=Counter(dict(currentRow[index]))
		        		collFeatures.update(ctrCurrent)
		        		#selecting only fixed keys from the current column		        		
		        		filtered=[(k,v) for k, v in ctrCurrent.items() if k in keys]
		        		filteredCollection.update(dict(filtered))	        		        	   		        
		        		entropies.append(entropy(collFeatures))
		        	finalCollectionFeatures.append(filteredCollection.values())
		        	entropyCollectionFeatures.append(entropies)		
		ffinalcollection=reduce(operator.add,finalCollectionFeatures)
		fcollentropy=reduce(operator.add,entropyCollectionFeatures)

		finalFeats=reduce(operator.add,[ffNumerical,ffEntropy,ffinalcollection,fcollentropy])
		print len(finalFeats)
		print finalFeats
		# Predicting and printing the value.
		#print model.predict(finalFeats[0:29])
		#The list of finalFeats can me multiplied to a list of  values here to get the values
	else:
		pass

sc = SparkContext("local[2]", "ParsingPcap")
sqlContext = SQLContext(sc)
ssc = StreamingContext(sc,0.5)
#logger = sc._jvm.org.apache.log4j
#logger.LogManager.getLogger("org").setLevel( logger.Level.OFF )
#logger.LogManager.getLogger("akka").setLevel( logger.Level.OFF )

myStreamRDD =ssc.socketTextStream("localhost", 11111)
lines = myStreamRDD.map(lambda line: line.encode("utf-8"))
dictionaries = lines.window(3.5,0.5)
dictionaries2=dictionaries.foreachRDD(rddOperation)

# Loading up the model
#modelpath="file:///home/ayush/Desktop/lab/lab_repo_bitbucket/Spark_pcap/json/data/models_BYTECOUNT/"
#model = LinearRegressionModel.load(sc, modelpath)

print "<<End"
ssc.start()
#ssc.awaitTermination()
ssc.awaitTerminationOrTimeout(15)
ssc.stop()