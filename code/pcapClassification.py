from pyspark.mllib.tree import RandomForest, RandomForestModel
from pyspark.mllib.util import MLUtils

from pyspark.sql.types import *
from pyspark.sql import Row
from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.mllib.regression import LabeledPoint, LinearRegressionWithSGD
from pyspark.mllib.util import MLUtils
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.linalg.distributed import IndexedRow, IndexedRowMatrix
from pyspark.mllib.linalg import DenseMatrix
import time
from pyspark.mllib.linalg.distributed import RowMatrix
from pyspark.mllib.linalg.distributed import Matrix
from pyspark.mllib.linalg import Matrices
import numpy as np
from numpy import linalg as la

def convertToLabelPoint(line):
	lString=line.split(",")
	result=[]
	for x in lString:
		result.append(float(x))
	return LabeledPoint(result[-1],tuple(result[0:59]))

path = "file:///home/ayush/Desktop/lab/lab_repo_bitbucket/spark_code/data/"
trainpath=path+"nn21rows_60features_anomalies_10_10.csv"
sc = SparkContext("local[2]", "ParsingPcap")
rawdata =sc.textFile(trainpath)
header=rawdata.first()

data = rawdata.filter(lambda x: x != header)
filterLines = data.map(lambda line: line.encode("utf-8"))
dataSet=filterLines.map(lambda x: convertToLabelPoint(x))
#dataSet=packetsNumber.map(lambda x: convertNumber(x))
#print dataSet.collect()

trainingData, testData = dataSet.randomSplit([0.7, 0.3])

model = RandomForest.trainClassifier(trainingData, numClasses=10, categoricalFeaturesInfo={},
                                     numTrees=3, featureSubsetStrategy="auto",
                                     impurity='gini', maxDepth=4, maxBins=32)

# Evaluate model on test instances and compute test error
predictions = model.predict(testData.map(lambda x: x.features))
labelsAndPredictions = testData.map(lambda lp: lp.label).zip(predictions)
testErr = labelsAndPredictions.filter(lambda (v, p): v != p).count() / float(testData.count())
print('Test Error = ' + str(testErr))
print('Learned classification forest model:')
#print(model.toDebugString())

# Save and load model
model.save(sc, path+"myRandomForestClassificationModel")
sameModel = RandomForestModel.load(sc, path+"myRandomForestClassificationModel")