# Details of the folder and files
# Author: Ayush Raj Aryal
# 29 June 2016

/data/
	- equinix-sanjose.dirB.20110120-140100.UTC.anon.pcap.gz : sample pcap gz file of 5seconds
	- sanjose.dirB.20110120-140100_0.5.txt : Features extracted from pcap file and represented as json (17 lines equivalent)
	- nn21rows_60features_anomalies_10_10.csv : Features file with 60 features and labels.
		sample1000.csv : sub sample of nn21rows_60features_anomalies_10_10.csv

/code/
pcapClassification.py :
	-It reads the 60 features from text file, creates a random forest model and does prediction test. [70/30 split]
pcapTextStream.py: 
	- Streaming linear regression model created using streaming from a text file
	- Input: Path of train and test folder.
	- Once the application is started, we need to copy the text files to train and test folder to get it processed.
pcapSocketStrem.py:
		- It takes streams from sockets and creates models.
pcapLinearRegression.py:
		- It is direct impementation of Linear Regression that Naresh did to spark.
pcapJsonParser.py:
	- It contains utility function  to convert the json (17 line equivalent) to equivalent features features.
	- This file also contains the list of features that is included in our features set and needs to be changed after finalizing features.
pcapExtractor.py:
	- It contains utility to genereate json (17 line equivalent). This has been implementend in C by deepak.
pcapStreamer.py: 
	- It is used to stream the lines to the spark application
pcapSocketClient.py : 
	- It is a simple python tcp  socket client for debugging purposes.
pcapDynamicIntegration.py:
	- This file takes streaming input from TCP sockets (sent using pcapStreamer.py).
	- It takes packets in 0.5 seconds time frame. The data is grouped in set of 7 packets and aggregated features are generated.
	- The features generated can be used for linear regression.
pcapStepAheadTrain.py :
	- It reads json, converts to features, and saves the model. The models are trained to predict labels after certain steps (n).
pcapStepAheadTest.py :
	- It reads json, converts to features, and computes prediction results using the model saved in (pcapStepAheadTrain.py)


Running the spark application:
	-- On local installation of machine you can run: pyspark <script name> >> log.out 
	-- On HPC enviroment
		Step 1. Run /scripts/runspark_1.sh to initiate the cluster.
		Step 2. Run /scripts/runspark_2.sh to submit and run the application.
				-- In runspark_2.sh we ned to give the path of spark library and also path of the script to submit.


